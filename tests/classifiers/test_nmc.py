import unittest
import numpy as np
from src.classifiers import NMC


class TestNMC(unittest.TestCase):

    def test_fit(self):
        """
        This test generates a set of labels and sets all feature values of x to
        the corresponding class label value. For example, if y=1, all x in this
        class will be [1 1 1 .... 1]. The corresponding centroid will thus be
        again equal to [1 1 1 .... 1]. This test thus checks if the centroids
        computed by fit are equivalent to the unique rows of the matrix x_tr.
        """
        n_classes = 5
        n_features = 10
        n_samples_class = 5

        # class_labels = np.random.randint(0, 100, n_classes)
        class_labels = np.random.choice(range(0, 100), n_classes)
        # print "class labels: ", class_labels

        x_tr = np.zeros((n_classes * n_samples_class, n_features))
        y_tr = np.zeros((n_classes * n_samples_class,))

        for i in xrange(n_classes):
            idx_start = i * n_samples_class
            idx_end = idx_start + n_samples_class
            x_tr[idx_start: idx_end, :] = class_labels[i]
            y_tr[idx_start: idx_end] = class_labels[i]

        # print "y: ", y_tr
        # print "x: ", x_tr

        nmc = NMC()
        nmc.fit(x_tr, y_tr)

        self.assertTrue((np.unique(x_tr, axis=0) == nmc.centroids).all())

    def test_predict(self):
        pass


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestNMC)
    unittest.TextTestRunner(verbosity=2).run(suite)
